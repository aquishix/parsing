

import java.util.List;
import java.util.regex.Pattern;

public class Token {
	public static enum Type { DOUBLE, ADD, MULTIPLY, SUBTRACT, DIVIDE, EXPONENTIATE, LEFT_PAREN, RIGHT_PAREN, NULL };

	public static enum TypeType { OPERAND, OPERATOR, NULL };  // This is pretty goofy.  Also, do I REALLY need a NULL item in here?
	
	public static final Pattern DOUBLE_PATTERN = Pattern.compile("\\d");
	
	public Type type = Type.NULL;
	public TypeType typeType = TypeType.NULL;
	
	public double doubleValue = 0.0;
	
	private String stringValue = "";
	
	public boolean isLeftAssociative() {
		switch (type) {
		case ADD:
			return true; 
		case MULTIPLY:
			return true; 
		case SUBTRACT:
			return true; 
		case DIVIDE:
			return true; 
		case EXPONENTIATE:
			return false; 
		case LEFT_PAREN:
			return true; // =/ 
		case RIGHT_PAREN:
			return true; // this could be better 
		default:
			return true; // this could be better
		}
	}
	
	public int precedence() {
		switch (type) {
		case ADD:
			return 1; 
		case MULTIPLY:
			return 2; 
		case SUBTRACT:
			return 1; 
		case DIVIDE:
			return 2; 
		case EXPONENTIATE:
			return 3; 
		case LEFT_PAREN:
			return Integer.MAX_VALUE; // =/ 
		case RIGHT_PAREN:
			return -1; // this could be better 
		default:
			return -1; // this could be better
		}
	}
	
	public String toString() {
		return stringValue; 
	}
	
	public Token() {
	}
	
	public Token(double value) {
		type = Type.DOUBLE;
		typeType = TypeType.OPERAND;
		doubleValue = value;
		stringValue = "" + value;
	}
	
	public Token(String rawString) { 
		stringValue = rawString;
		if ("+".equals(rawString)) {
			type = Type.ADD;
			typeType = TypeType.OPERATOR;
		}
		else if ("*".equals(rawString)) {
			type = Type.MULTIPLY;
			typeType = TypeType.OPERATOR;
		}
		else if ("-".equals(rawString)) {
			type = Type.SUBTRACT;
			typeType = TypeType.OPERATOR;
			}
		else if ("/".equals(rawString)) {
			typeType = TypeType.OPERATOR;
			type = Type.DIVIDE;
			}
		else if ("^".equals(rawString)) {
			typeType = TypeType.OPERATOR;
			type = Type.EXPONENTIATE;
			}
		else if ("(".equals(rawString)) {
			typeType = TypeType.OPERATOR;
			type = Type.LEFT_PAREN;
			}
		else if (")".equals(rawString)) {
			typeType = TypeType.OPERATOR;
			type = Type.RIGHT_PAREN;
			}
		else if (DOUBLE_PATTERN.matcher(rawString).find()) {
			type = Type.DOUBLE;
			typeType = TypeType.OPERAND;
			doubleValue = Double.parseDouble(rawString);
		}
		else {
			type = Type.NULL;
			typeType = TypeType.NULL;
		}
	}
	
	public static void printTokenList(List<Token> tokenList) {
		for (Token t : tokenList) {
			System.out.print(t.toString() + " ");
		}
	}
	
	public static void printTokenListVerbose(List<Token> tokenList) {
		for (Token t : tokenList) {
			System.out.println(t.toString() + " -- type: " + t.type);
		}
	}
	
	public static void main(String[] args) {
		printTokenListVerbose(TrivialLexer.trivialLexer("3 5 + / * - a ( )"));
	}
}
