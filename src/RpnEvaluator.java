import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;

public class RpnEvaluator {
	private static double result = 0.0;
	private static Stack<Token> tokenStack = new Stack<Token>();  // strictly speaking, this ought to be a stack of operands =(
	private static List<Token> tokenList = new ArrayList<Token>();
	private static Token popped0 = new Token();
	private static Token popped1 = new Token();
	
	
	public static double evaluator(List<Token> tokenList) throws Exception {
		double toReturn = 0.0;
		for (Token t : tokenList) {
			if (t.typeType == Token.TypeType.NULL) throw new Exception("Token of TypeType NULL encountered!"); 
			if (t.typeType == Token.TypeType.OPERAND) tokenStack.push(t);
			else {  // I.e., it's an OPERATOR!
				popped0 = tokenStack.pop();
				popped1 = tokenStack.pop();
				switch (t.type) {  // RTL: Refactor this shit to a generalized eval(-,-,-) function!
				case ADD:
					tokenStack.push(new Token(popped1.doubleValue + popped0.doubleValue));
					break;
				case MULTIPLY:
					tokenStack.push(new Token(popped1.doubleValue * popped0.doubleValue));
					break;
				case SUBTRACT:
					tokenStack.push(new Token(popped1.doubleValue - popped0.doubleValue));
					break;
				case DIVIDE:
					tokenStack.push(new Token(popped1.doubleValue / popped0.doubleValue));
					break;
				case EXPONENTIATE:
					tokenStack.push(new Token(Math.pow(popped1.doubleValue, popped0.doubleValue)));
					break;
				default:
					break;
				}
			} // end else
		}
		toReturn = tokenStack.pop().doubleValue;
		return toReturn;
	}
	
	
	
	private static void evaluateTestData0() throws Exception {
		String testData0 = "3 5 7 * *";
		tokenList = TrivialLexer.trivialLexer(testData0);
		result = evaluator(tokenList);
		System.out.println("Result is: " + result);
	}
	
	private static void evaluateUserInput() throws Exception {
		Scanner scanner = new Scanner(System.in);
		String userInput = "";
		while (true) {
			System.out.print("Input RPN expression: ");
			userInput = scanner.nextLine();
			result = evaluator(TrivialLexer.trivialLexer(userInput));
			System.out.println("result: " + result);
		}
	}
	
	public static void main(String[] args) throws Exception {
		evaluateUserInput();
		//evaluateTestData0();
		
	}

}
