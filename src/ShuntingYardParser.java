import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;


public class ShuntingYardParser {
	private static Stack<Token> operatorStack = new Stack<Token>();  // strictly speaking ...
	private static Token popped0 = new Token();
	private static Token popped1 = new Token();
	
	public static List<Token> parser(List<Token> inputList) throws Exception {
		List<Token> outputList = new ArrayList<Token>();
		operatorStack.clear();
		
		for (Token t : inputList) {
			if (t.typeType == Token.TypeType.OPERAND) {
				outputList.add(t);
				continue;
			}
			if (t.typeType == Token.TypeType.OPERATOR) {
				if (t.type == Token.Type.LEFT_PAREN) {
					operatorStack.push(t);
					continue;
				}
				if (t.type == Token.Type.RIGHT_PAREN) {
					while (!operatorStack.empty()) {
						popped0 = operatorStack.pop();
						if (popped0.type == Token.Type.LEFT_PAREN) {
							//breakOut = true;
							break;
						}
						outputList.add(popped0);
					}
					continue;
				}
				
				if (operatorStack.empty()) {
					operatorStack.push(t);
					continue;
				}
				
				if (operatorStack.peek().type == Token.Type.LEFT_PAREN) {
					operatorStack.push(t);
					continue;
				}
				
				if (t.precedence() > operatorStack.peek().precedence()) {
					operatorStack.push(t);
					continue;
				}
				
				// 1 + 2 * ( 3 * 4 - 5 ) + 6
				boolean emptiedTheList = false;
				while (!operatorStack.empty()) {
					if (operatorStack.peek().type == Token.Type.LEFT_PAREN) {
						operatorStack.push(t);
						break;
					}
					
					if (t.precedence() <= operatorStack.peek().precedence()) {
						if (!t.isLeftAssociative() && !operatorStack.peek().isLeftAssociative()) {
							operatorStack.push(t);
							break;
						}
						else {
							outputList.add(operatorStack.pop());
							if (operatorStack.empty()) emptiedTheList = true;  // this feels like an ugly hack, but it works =(
							continue;	
						}
						
					}
					
					operatorStack.push(t);
					break;
					
					
				} // end while loop
				if (emptiedTheList) operatorStack.push(t);
			} // end consideration of OPERATOR token
			
		} // end main for loop
		while (!operatorStack.empty()) {
			popped0 = operatorStack.pop();
			//System.out.println("popped token: " + popped0.toString());
			outputList.add(popped0);
			
		} // end while loop
		
		return outputList;
	}
	
	private static void parseUserInput() throws Exception {
		Scanner scanner = new Scanner(System.in);
		String userInput = "";
		while (true) {
			System.out.print("Input infix expression: ");
			userInput = scanner.nextLine();
			List<Token> resultList = parser(TrivialLexer.trivialLexer(userInput));
			
			System.out.println("result: ");
			Token.printTokenList(resultList);
			System.out.println("\n");
			System.out.println("Evaluation: ");
			System.out.println("" + RpnEvaluator.evaluator(resultList));
		}
	}
		
	public static void main(String[] args) throws Exception {
		parseUserInput();
	}

}
