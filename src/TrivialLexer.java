
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TrivialLexer {
 
	public static ArrayList<Token> trivialLexer(String inputString) {
         List<String> inputList = Arrays.asList(inputString.split(" "));
         ArrayList<Token> toReturn = new ArrayList<Token>();
         for (String s : inputList) {
             Token t = new Token(s);
        	 toReturn.add(t);
             //System.out.println("Adding token: " + t.toString());
         }
         return toReturn;
     }

	
	public static void main(String[] args) {
		
	}

}
